import React, { useEffect, useState } from "react";
import { StyleSheet, View, Text} from "react-native";
/* types */
import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "../types/navigation";

type Props = {
  navigation: StackNavigationProp<RootStackParamList, "Top">;
};

export const TopScreen = ({ navigation }: Props) => {

  const onPress = () => {
    navigation.navigate("Auth");
  };

  return (
    <View style={styles.container}>
      <Text onPress={onPress}>This is Top</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

