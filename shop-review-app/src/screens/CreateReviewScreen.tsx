import React, { useEffect, useState, useContext } from "react";
import { StyleSheet, SafeAreaView, View, Image, Alert, Text } from "react-native";
import firebase from "firebase";
import { createReviewRef, uploadImage } from "../lib/firebase";
import { pickImage } from "../lib/image-picker";
import { takeCamera } from "../lib/image-picker";
import { UserContext } from "../contexts/userContext";
import { ReviewsContext } from "../contexts/reviewsContext";
import { getExtension } from "../utils/file";
/* components */
import { IconButton } from "../components/IconButton";
import { TextArea } from "../components/TextArea";
import { StarInput } from "../components/StarInput";
import { Button } from "../components/Button";
import { Loading } from "../components/Loading";
/* types */
import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "../types/navigation";
import { RouteProp } from "@react-navigation/native";
import { Review } from "../types/review";
import { Dialog, ProgressDialog, ConfirmDialog } from "react-native-simple-dialogs";

type Props = {
  navigation: StackNavigationProp<RootStackParamList, "CreateReview">;
  route: RouteProp<RootStackParamList, "CreateReview">;
};

export const CreateReviewScreen: React.FC<Props> = ({
  navigation,
  route,
}: Props) => {
  const { shop } = route.params;
  const [score, setScore] = useState<number>(3);
  const [imageUri, setImageUri] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const [progressVisible, setProgressVisible] = useState<boolean>(false);
  const [showDialog, setShowDialog] = useState<boolean>(false);
  const { user } = useContext(UserContext);
  const { reviews, setReviews } = useContext(ReviewsContext);

  useEffect(() => {
    navigation.setOptions({
      title: shop.name,
      headerLeft: () => (
        <IconButton name="x" onPress={() => navigation.goBack()} />
      ),
    });
  }, [shop]);

  const getMoviesFromApiAsync = async () => {
    try {
      let response = await fetch(
        'https://reactnative.dev/movies.json'
      );
      let json = await response.json();
      return json.movies;
    } catch (error) {
      console.error(error);
    }
  };

  function sleep(waitSec, callbackFunc) {

    var spanedSec = 0;
  
    var waitFunc = function () {
  
        spanedSec++;
  
        if (spanedSec >= waitSec) {
            if (callbackFunc) callbackFunc();
            return;
        }
  
        clearTimeout(id);
        id = setTimeout(waitFunc, 1000);
    
    };
  
    var id = setTimeout(waitFunc, 1000);
  
  }

  const onSubmit = async () => {
    if (!imageUri) {
      Alert.alert("画像がありません");
      return;
    }

    // var request = new XMLHttpRequest();
    // request.onreadystatechange = (e) => {
    //   if (request.readyState !== 4) {
    //     return;
    //   }

    //   if (request.status === 200) {
    //     console.log('success', request.responseText);
    //   } else {
    //     console.warn('error');
    //   }
    // };

    // request.open('GET', 'https://google-translate1.p.rapidapi.com/language/translate/v2/languages');
    // request.send();
    const sample = await getMoviesFromApiAsync();
    console.log("test");
    console.log(imageUri);
    console.log(sample);
    let is_correct = true
    if (!is_correct) return;
    setProgressVisible(true);
    
    sleep(5, function() {
      console.log('5秒経過しました！');
      setProgressVisible(false);
      setShowDialog(true);
    });
    // setProgressVisible(false);
    // setShowDialog(true);

    // setLoading(true);
    // // documentのIDを先に取得
    // const reviewDocRef = await createReviewRef(shop.id);
    // // storageのpathを決定
    // const ext = getExtension(imageUri);
    // const storagePath = `reviews/${reviewDocRef.id}.${ext}`;
    // // 画像をstorageにアップロード
    // const downloadUrl = await uploadImage(imageUri, storagePath);
    // // reviewドキュメントを作る
    // const review = {
    //   id: reviewDocRef.id,
    //   user: {
    //     name: user.name,
    //     id: user.id,
    //   },
    //   shop: {
    //     name: shop.name,
    //     id: shop.id,
    //   },
    //   imageUrl: downloadUrl,
    //   updatedAt: firebase.firestore.Timestamp.now(),
    //   createdAt: firebase.firestore.Timestamp.now(),
    // } as Review;
    // await reviewDocRef.set(review);
    // // レビュー一覧に即時反映する
    // setReviews([review, ...reviews]);

    // setLoading(false);
    // navigation.navigate('Complete')
  };

  const onMock = async () => {
    setShowDialog(false);
    setLoading(true);
    // documentのIDを先に取得
    const reviewDocRef = await createReviewRef(shop.id);
    // storageのpathを決定
    const ext = getExtension(imageUri);
    const storagePath = `reviews/${reviewDocRef.id}.${ext}`;
    // 画像をstorageにアップロード
    const downloadUrl = await uploadImage(imageUri, storagePath);
    // reviewドキュメントを作る
    const review = {
      id: reviewDocRef.id,
      user: {
        name: user.name,
        id: user.id,
      },
      shop: {
        name: shop.name,
        id: shop.id,
      },
      imageUrl: downloadUrl,
      updatedAt: firebase.firestore.Timestamp.now(),
      createdAt: firebase.firestore.Timestamp.now(),
    } as Review;
    await reviewDocRef.set(review);
    // レビュー一覧に即時反映する
    setReviews([review, ...reviews]);

    setLoading(false);
    navigation.navigate('Complete')
  };

  const onPickImage = async () => {
    const uri = await takeCamera();
    setImageUri(uri);
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.photoContainer}>
        <IconButton name="camera" onPress={onPickImage} color="#ccc" />
        {!!imageUri && (
          <Image source={{ uri: imageUri }} style={styles.image} />
        )}
      </View>
      <Button text="写真を送信" onPress={onSubmit} />
      <Loading visible={loading} />
      <Dialog
          title="判定結果"
          animationType="fade"
          contentStyle={
              {
                  alignItems: "center",
                  justifyContent: "center",
              }
          }
          visible={ showDialog }
      >
      <Text style={ { marginVertical: 30 } }>
          ウォーリーが検出されました。クエストクリアです。
      </Text>
      <Button text="CLOSE" onPress={onMock} />
      </Dialog>
      <ProgressDialog
          visible={progressVisible}
          title="判定中"
          message="少々お待ちください"
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  photoContainer: {
    margin: 8,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 300,
    height: 300,
    margin: 8,
  },
});
