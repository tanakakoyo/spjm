import React, { useState } from 'react'
import BasicModalComponent from '../components/BasicModal'
import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "../types/navigation";

type Props = {
  navigation: StackNavigationProp<RootStackParamList, "CreateReview">;
};
const CompleteScreen: React.FunctionComponent<Props> = ({ navigation }) => {
  const [modalVisible, setModalVisible] = useState(true)
  const modalText = 'クエスト完了'

  setTimeout(() => {
    setModalVisible(false)
    navigation.navigate('Home')
  }, 2000)

  return (
    <BasicModalComponent modalVisible={modalVisible} modalText={modalText} />
  )
}

export default CompleteScreen