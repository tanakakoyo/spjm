import React from "react";
import { View, StyleSheet, Image, Text } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
/* components */
import { Stars } from "./Stars";
/* types */
import { Shop } from "../types/shop";

type Props = {
  shop: Shop;
};

export const ShopDetail: React.FC<Props> = ({ shop }: Props) => {
  const { name, place, imageUrl, score } = shop;

  return (
    <View style={styles.container}>
      <View style={styles.image}>
        <Image style={styles.image} source={{ uri: imageUrl }}></Image>
        <LinearGradient
          colors={["transparent", "rgba(0,0,0,0.8)"]}
          style={styles.gradient}
        />
        <View style={styles.nameContainer}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.place}>{place}</Text>
        </View>
      </View>
      <View style={styles.starContainer}>
        <Stars score={score} starSize={28} textSize={20} />
      </View> 
      <Text style={styles.header}>クエスト内容</Text>
      <Text style={styles.description}>ウォーリーの仮装をした人を探し、一緒に写真を撮りましょう。Wikipediaによると、ウォーリーは魔法使いのおじいさんから依頼を受け、おじいさんから託された魔法の杖で異世界の事件を解決していきます。並大抵のことでは驚いたり、怖気づいたりしないタフな精神領域を持ちます。</Text>
      <Text style={styles.header}>クエストの進め方</Text>
      <Text style={styles.description}>①下部の「クエストを開始」を押して、クエストを始めます。</Text>
      <Text style={styles.description}>②クエスト内容にあう、ウォーリーを探します。</Text>
      <Text style={styles.description}>③ウォーリーを見つけたら、カメラマークを押して一緒に写真をとります。</Text>
      <Text style={styles.description}>④撮った写真をアップロードして、クリアとなれば完了です。
※画像判定により、ウォーリーが入っているか判定します。</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageContainer: {
    width: "100%",
    height: 250,
    resizeMode: "cover",
  },
  nameContainer: {
    position: "absolute",
    left: 16,
    bottom: 16,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "flex-end",
  },
  starContainer: {
    margin: 16,
  },
  image: {
    width: "100%",
    height: 250,
    resizeMode: "cover",
  },
  name: {
    fontSize: 20,
    color: "#fff",
    fontWeight: "bold",
  },
  place: {
    fontSize: 16,
    color: "#fff",
    fontWeight: "bold",
    marginLeft: 16,
  },
  gradient: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    height: 250,
  },
  header: {
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: 16,
    marginTop: 10,
  },
  description: {
    fontSize: 18,
    color: "grey",
    marginTop: 5,
    marginLeft: 26,
    marginRight: 26,
  },
});
