import React from 'react'
import { Modal, StyleSheet, Text, View } from 'react-native'

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000000'
  },
  modalText: {
    fontSize: 36,
    fontWeight: 'bold'
  },
  modalWrapper: {
    backgroundColor: '#FFFFFF',
    height: 200,
    width: 200,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
})

const BasicModal = props => {
  const { modalText, modalVisible } = props
  return (
    <Modal animationType="slide" transparent={true} visible={modalVisible}>
      <View style={styles.modalBackground}>
        <View style={styles.modalWrapper}>
          <Text style={styles.modalText} data-test="basicModalText">
            {modalText}
          </Text>
        </View>
      </View>
    </Modal>
  )
}

export default BasicModal
