import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import Constants from "expo-constants";

const getCameraRollPermission = async () => {
  if (Constants.platform.ios) {
    const { status } = await ImagePicker.getCameraRollPermissionsAsync();
    if (status !== "granted") {
      alert("画像を選択するためにはカメラロールの許可が必要です");
    }
  }
};

const getCameraPermission = async () => {
  if (Constants.platform.ios) {
  await Permissions.askAsync(Permissions.CAMERA);
  const { status } = await ImagePicker.getCameraPermissionsAsync();
  console.log(status);
    if (status !== "granted") {
      alert("撮影にはカメラの許可が必要です");
    }
  }
};

export const pickImage = async () => {
  // パーミッションを取得
  await getCameraRollPermission();
  // ImagePicker起動
  const result = await ImagePicker.launchImageLibraryAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.Images,
    allowsEditing: false,
  });
  if (!result.cancelled) {
    return result.uri;
  }
};

export const takeCamera = async () => {
  // パーミッションを取得
  await getCameraPermission();
  // カメラの起動
  const result = await ImagePicker.launchCameraAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.Images,
    allowsEditing: false,
  });
  if (!result.cancelled) {
    return result.uri;
  }
};

