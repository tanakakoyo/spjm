# spjm

## 謝辞

下記のサンプルコードを参考に作成させていただきました。
![udemy-firebase-banner_400](https://user-images.githubusercontent.com/7026785/90077348-e0f2b480-dd3c-11ea-99e7-f0826ceeb091.png)
Udemy講座 **「React NativeとFirebaseで作るiOS/Androidアプリ：お店レビューアプリ開発編」** のサンプルコードです。


## 構成

```
├── assets  // サンプルデータで使う画像置き場(実際のアプリでは使用しません)
├── shop-review-app  // React Nativeのプロジェクト
└── shop-review-firebase  // Firebase Cloud Function
```

## 環境構築手順

### 必要なライブラリのインストール
```
# homebrewのインストール
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# 確認
brew -v


# Gitのインストール
brew install git

# anyenvのインストール
brew install anyenv
# 初期化
anyenv init
# 設定ファイルに追記
echo 'eval "$(anyenv init -)"' >> ~/.zshrc  # 使ってるshellによって分けてください
echo 'eval "$(anyenv init -)"' >> ~/.bashrc # 使ってるshellによって分けてください


# マニフェストファイルの作成
anyenv install --init

# nodenvのインストール
anyenv install nodenv

# 環境変数の再起動
exec $SHELL -l

# node バージョン10.19.0のインストール
nodenv install 10.19.0
# デフォルトに設定
nodenv global 10.19.0

# グローバルに使う関連パッケージのインストール
npm install -g yarn
npm install -g expo-cli
npm install -g firebase-tools

# サンプルアプリのクローン
git clone git@github.com:mopinfish/spjm-sample.git


```

### アプリの初期化
```
cd spjm
cd shop-review-app
yarn
```

また下記コマンドでapp.jsonを作成する

```
touch app.json
# 中身に記載する情報は別途お伺いください
```

# 下記コマンドでアプリが起動したら成功
expo start

```

## セクション3で使うFirestoreサンプルデータ

ドキュメントID | name (string) | place (string) | imageUrl (string) | score (number) |
-- | -- | -- | -- | -- |
10000 | 有楽町カフェ | 有楽町 | https://raw.githubusercontent.com/takahi5/shop-review/master/assets/cafe01.jpg | 3 |
10001 | 新橋パスタ | 新橋 | https://raw.githubusercontent.com/takahi5/shop-review/master/assets/pasta01.jpg | 4 |
10002 | ピッツア浜松町 | 浜松町 | https://raw.githubusercontent.com/takahi5/shop-review/master/assets/pizza01.jpg | 3.5 |
10003 | 田町ラーメン | 田町 | https://raw.githubusercontent.com/takahi5/shop-review/master/assets/ramen01.jpg | 4.5 |
10004 | ビストロ品川 | 品川 | https://raw.githubusercontent.com/takahi5/shop-review/master/assets/meat01.jpg | 2 |

## 各レクチャーの途中のコード

本リポジトリのコードは最終的な完成形になっています。

各レクチャーの途中経過のコードについては、ブランチを切って置いていますので、適宜参考にして下さい。

例：セクション4 AppNavigatorの導入 → https://github.com/takahi5/shop-review/tree/sec4/app-navigator

**ブランチ一覧**    
https://github.com/takahi5/shop-review/branches/active
